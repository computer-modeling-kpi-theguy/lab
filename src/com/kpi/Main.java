package com.kpi;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double mu, lambda, num;
        do {
            System.out.println("Enter the mu of the SMO:");
            mu = scan.nextDouble();
            System.out.println("Enter the lambda of the SMO:");
            lambda = scan.nextDouble();
            System.out.println("Enter the border for uniform distribution of the generator:");
            num = scan.nextDouble();
            if(lambda > mu || (1/mu - num/2) < 0)
                System.out.println("You entered impossible data to build the SMO");
            else
                break;
        }while(true);
        System.out.println("Enter the number of tasks for calculation of the SMO:");
        int tasks = scan.nextInt();
        System.out.println("Working disciplines: ");
        System.out.println("FIFO System ");
        System.out.println("========================");
        SMO_FIFO smo1 = new SMO_FIFO(mu,lambda,tasks,num);
        smo1.start_system();

        System.out.println("Average time in system: " + smo1.getAverageTime());
        System.out.println("Average dispersion: "+ smo1.getDispersionTime());
        System.out.println("Average time of reaction: " + smo1.getReactionTime());
        System.out.println("Result Function: " + smo1.getResultFunction());
        System.out.println("========================");
        System.out.println("Average time in system of 50 tasks: " + smo1.getAverageTime50());
        System.out.println("Average dispersion of 50 tasks: "+ smo1.getDispersionTime50());
        System.out.println("Average time of reaction of 50 tasks: " + smo1.getReactionTime50());
        System.out.println("Result Function of 50 tasks: " + smo1.getResultFunction50());
        System.out.println("========================");
        System.out.println("FB System ");
        double expect_tau = 1/mu - num/2 + 0.001;
        double i_expect_tau = 1/mu - num/2 + 0.002;
        boolean flag = false;
        DecimalFormat formatter = new DecimalFormat("0.000");
        while(!flag){
            SMO_FB test_smo = new SMO_FB(mu,lambda,tasks,num,expect_tau);
            SMO_FB test_smo2 = new SMO_FB(mu,lambda,tasks,num, i_expect_tau);
            test_smo.start_system();
            test_smo2.start_system();
            double buff1 = test_smo.getResultFunction();
            double buff2 = test_smo2.getResultFunction();
            if(buff2 > buff1)
                expect_tau = i_expect_tau;
            i_expect_tau += 0.001;
            i_expect_tau = Double.parseDouble(formatter.format(i_expect_tau).replace(',','.'));
            if (i_expect_tau == 1/mu + num/2)
                flag = true;
        }
        SMO_FB smo2 = new SMO_FB(mu,lambda,tasks,num,expect_tau);
        smo2.start_system();
        System.out.println("The best tau for discipline: " + expect_tau);
        System.out.println("Average time in system: " + smo2.getAverageTime());
        System.out.println("Average dispersion: "+ smo2.getDispersionTime());
        System.out.println("Average time of reaction: " + smo2.getReactionTime());
        System.out.println("Result Function: " + smo2.getResultFunction());
        System.out.println("========================");
        System.out.println("Average time in system 0f 50 tasks: " + smo2.getAverageTime50());
        System.out.println("Average dispersion of 50 tasks: "+ smo2.getDispersionTime50());
        System.out.println("Average time of reaction of 50 tasks: " + smo2.getReactionTime50());
        System.out.println("Result Function of 50 tasks: " + smo2.getResultFunction50());
    }
}

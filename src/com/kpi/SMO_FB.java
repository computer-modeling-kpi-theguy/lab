package com.kpi;

import java.util.ArrayList;
import java.util.HashMap;

public class SMO_FB extends SMO {
    public SMO_FB(double mu, double lambda, int iter, double num, double tau) {
        super(mu, lambda, iter, num);
        this.tau = tau;
    }

    private double tau;
    private HashMap <Integer,ArrayList<Task>> queses = new HashMap<>();
    private int currentPosition = 0;

    @Override
    public void calculateTask() {
        Task calculateTask;
        if (getQueue().size() != 0){
            calculateTask = getQueue().get(0);
            if (calculateTask.getTime() < tau){
                setTimeToCalculate(calculateTask.getTime());
                getQueue().remove(calculateTask);
                calculateTask.setTimeCompute(getFullTime() + calculateTask.getTime());
                getCompletedQueue().add(calculateTask);
            }
            else{
                setTimeToCalculate(tau);
                getQueue().remove(calculateTask);
                calculateTask.setTime(calculateTask.getTime() - tau);
                queses.computeIfAbsent(0, k -> new ArrayList<>());
                queses.get(0).add(calculateTask);
                currentPosition  = 0;
            }
        }
        else{
            if(!queses.isEmpty()) {
                if (queses.get(currentPosition) == null) {
                    currentPosition++;
                } else {
                    calculateTask = queses.get(currentPosition).get(0);
                    if (calculateTask.getTime() < tau) {
                        setTimeToCalculate(calculateTask.getTime());
                        queses.get(currentPosition).remove(calculateTask);
                        calculateTask.setTimeCompute(getFullTime() + calculateTask.getTime());
                        getCompletedQueue().add(calculateTask);
                    } else {
                        if (calculateTask.getTime() == tau){
                            setTimeToCalculate(tau);
                            queses.get(currentPosition).remove(calculateTask);
                            calculateTask.setTimeCompute(getFullTime() + tau);
                            getCompletedQueue().add(calculateTask);
                        } else {
                            setTimeToCalculate(tau);
                            queses.get(currentPosition).remove(calculateTask);
                            calculateTask.setTime(calculateTask.getTime() - tau);
                            queses.computeIfAbsent(currentPosition + 1, k -> new ArrayList<>());
                            queses.get(currentPosition + 1).add(calculateTask);
                        }
                    }
                    if(queses.get(currentPosition).isEmpty())
                        queses.remove(currentPosition);
                }
            }
        }
    }
}

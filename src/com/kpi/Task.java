package com.kpi;

public class Task {

    private int id;
    private double time;
    private double timeStart;
    private double timeCompute;

    public Task(int id, double time){
        this.id = id;
        this.time = time;
    }

    public double timeInSystem(){
        double ret = timeCompute - timeStart;
        return ret;
    }

    public void setTimeStart(double timeStart) {
        this.timeStart = timeStart;
    }

    public void setTimeCompute(double timeCompute) {
        this.timeCompute = timeCompute;
    }

    public double getTime() {
        return time;
    }

    public int getId() {
        return id;
    }

    public void setTime(double time) {
        this.time = time;
    }
}

package com.kpi;

import java.util.ArrayList;

public abstract class SMO {

    private int tasks;

    private double num;
    private double mu;
    private double lambda;

    private int k1 = -3;
    private int k2 = -1;
    private int k3 = -6;

    private double fullTime;
    private int count;

    private ArrayList<Task> queue;
    private ArrayList<Task> completedQueue;
    private ArrayList<Task> completedQueue50;

    private double averageTime;
    private double dispersionTime;
    private double reactionTime;
    private double resultFunction;

    private double averageTime50;
    private double dispersionTime50;
    private double reactionTime50;
    private double resultFunction50;

    private double timeToGenerate;
    private double timeToCalculate;

    public abstract void calculateTask();

    public SMO(double mu, double lambda, int iter, double num){
        this.mu = mu;
        this.lambda = lambda;
        tasks = iter;
        this.num = num;
        queue = new ArrayList<>();
        completedQueue = new ArrayList<>();
        completedQueue50 = new ArrayList<>();
    }

    public void start_system(){
        while (completedQueue.size() != tasks) {
            double timeToNextOperation = 0;
            if (timeToGenerate <= timeToCalculate) {
                timeToNextOperation = timeToGenerate;
                fullTime += timeToNextOperation;
                timeToCalculate -= timeToNextOperation;
                generateTask();
            } else {
                timeToNextOperation = timeToCalculate;
                fullTime += timeToNextOperation;
                timeToGenerate -= timeToNextOperation;
                calculateTask();
            }
        }
        for (int i = 0; i < completedQueue.size(); i++) {
            if ((completedQueue.get(i).getId() % 50) == 0)
                completedQueue50.add(completedQueue.get(i));
        }
            averageTime = averageTimeInSystem(completedQueue);
            dispersionTime = dispersionInSystem(averageTime, completedQueue);
            reactionTime = timeOfReactionInSystem(completedQueue);
            resultFunction = k1*averageTime + k2*dispersionTime + k3*reactionTime;

            averageTime50 = averageTimeInSystem(completedQueue50);
            dispersionTime50 = dispersionInSystem(averageTime50, completedQueue50);
            reactionTime50 = timeOfReactionInSystem(completedQueue50);
            resultFunction50 = k1*averageTime50 + k2*dispersionTime50 + k3*reactionTime50;
    }

    protected void generateTask(){
        count++;
        double time;
        if ((count % 50) == 0){
            time = 2/mu;
        }
        else{
            time = (1/mu) + (Math.random() * 0.5) * num;
        }
        Task task = new Task(count,time);
        task.setTimeStart(fullTime);
        queue.add(task);
        timeToGenerate = (-1/lambda) * Math.log(Math.random());
    }

    protected double averageTimeInSystem(ArrayList<Task> array){
        double result = 0;
        for(Task task: array)
            result += task.timeInSystem();
        result /= array.size();
        return result;
    }

    protected double dispersionInSystem(double averageTime, ArrayList<Task> array){
        double result = 0;
        for(Task task: array)
            result += Math.pow(task.timeInSystem() - averageTime, 2);
        result /= array.size();
        return result;
    }

    protected double timeOfReactionInSystem(ArrayList<Task> array){
        double result = 0;
        for(Task task: array)
            result += task.timeInSystem() - task.getTime();
        result /= array.size();
        return result;
    }

    public ArrayList<Task> getQueue() {
        return queue;
    }

    public ArrayList<Task> getCompletedQueue() {
        return completedQueue;
    }

    public double getFullTime() {
        return fullTime;
    }

    public void setTimeToCalculate(double timeToCalculate) {
        this.timeToCalculate = timeToCalculate;
    }

    public double getAverageTime() {
        return averageTime;
    }

    public double getDispersionTime() {
        return dispersionTime;
    }

    public double getReactionTime() {
        return reactionTime;
    }

    public double getResultFunction() {
        return resultFunction;
    }

    public double getAverageTime50() {
        return averageTime50;
    }

    public double getDispersionTime50() {
        return dispersionTime50;
    }

    public double getReactionTime50() {
        return reactionTime50;
    }

    public double getResultFunction50() {
        return resultFunction50;
    }
}

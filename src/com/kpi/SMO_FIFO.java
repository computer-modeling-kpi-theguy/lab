package com.kpi;

public class SMO_FIFO extends SMO {

    public SMO_FIFO(double mu, double lambda, int iter, double num) {
        super(mu, lambda, iter, num);
    }

    @Override
    public void calculateTask() {
        Task calculateTask;
        if (getQueue().size() != 0){
            calculateTask = getQueue().get(0);
            setTimeToCalculate(calculateTask.getTime());
            getQueue().remove(calculateTask);
            calculateTask.setTimeCompute(getFullTime() + calculateTask.getTime());
            getCompletedQueue().add(calculateTask);
        }
    }
}

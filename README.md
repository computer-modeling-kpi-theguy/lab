Laboratory work #1 for Computer Modelling.

Purpose for the work: To study the Queueing theory inside of the computer model, and different algorithms, that describes different task behaviours inside that system.
Programming language: Java
Variant of the developed systems: 
FIFO - First In, First Out. All tasks works as simple queue, when system is ready to process the task, first from queue enter inside, and leave after full calculating of it.
FB - Multilevel Queue with time factor. All tasks works as multilevel queue with time factor. Firstly system calculates an optimal static time for calculation of the task. After this, every task calculates exactly that time, if it is enough - task is complete. In other way, time that need for calculation reduces by static time, and task goes to second queue. This repeates so mane time, until the task will be fully processed by CPU. The first queue has upper priority to calculate tasks then others, and priority reduces by next levels queue.

Task for the Laboratory work: Create the Programing model of the system with Queues that has that types of disciplines, described in the Variant. Calculate the statistics of created systems, which differs according to the entered characteristics of them.

Controling of the program:
variable mu - the center of distribution. Used as value of 1/mu, so it can be equal to 0.
variable lambda - coefficient for the time to generate the next value of the time for task. Must not be upper than mu, because it will be the impossible system with Queueing model. 
variable num - the borders of the distribution, that has characterstics of the mu and lambda.
variable tasks - number of the tasks, that will have that time characteristics, that comes from the distribution created by mu, lambda and num.

Program output as result that values: Average time in system, Average dispersion, Average time of reaction, Result function and it's values for 50 tasks.